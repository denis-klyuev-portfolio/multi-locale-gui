const isDevMode = process.env.NODE_ENV == 'development';
const {app, BrowserWindow} = require('electron');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;

function createTrayIcon() {
	const {Tray} = require('electron');
	const {Menu} = require('electron');
	const path = require('path');
	var trayIcon = new Tray(path.join(__dirname, 'img/tray-icon.png'));
	var trayMenuTemplate = [
		{
			label: 'Sound machine',
			enabled: false
		},
		{
			label: 'Settings',
			click: function () {
				ipc.send('open-settings-window');
			}
		},
		{
			label: 'Quit',
			click: function () {
				ipc.send('close-main-window');
			}
		}
	];
	const trayMenu = Menu.buildFromTemplate(trayMenuTemplate);
	trayIcon.setContextMenu(trayMenu);
}

function createWindow() {
	createTrayIcon();

	// Create the browser window.
	win = new BrowserWindow({
		width: 1200,
		height: 800,
		webSecurity: false
	});

	if (isDevMode) {
		const host = process.env.HOST || 'localhost';
		const protocol = process.env.HTTPS === 'true' ? "https" : "http";
		const port = 3000;
		win.loadURL(`${protocol}://${host}:${port}/index.html`);
		const installer = require('electron-devtools-installer');
		const extensions = [
			'REACT_DEVELOPER_TOOLS',
			'REDUX_DEVTOOLS'
		];
		const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
		for (const name of extensions) {
			try {
				installer.default(installer[name], forceDownload);
			} catch (e) {
			} // eslint-disable-line
		}
	} else {
		win.loadURL(`file://${__dirname}/index.html`);
	}

	// Open the DevTools.
	isDevMode && win.webContents.openDevTools();

	// Emitted when the window is closed.
	win.on('closed', () => {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		win = null;
	});
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
	// On macOS it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	// On macOS it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (win === null) {
		createWindow();
	}
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.