## Multi Locale GUI - редактор локализаций

Это прикладная программа, которая позволяет редактировать локализации для игр и движков в формате XML.

### Установка
Скачайте одну из сборок из последнего коммита (в виде артефактов к коммиту).

Сборки доступны для:
- Linux
- Mac
- Windows

### Разработка
#### Вариант с yarn (рекомендуется)
В первом терминале:
```
yarn
yarn start
```
Во втором терминале:
```
yarn run start-app-dev
```
#### Вариант с npm
В первом терминале:
```
npm install
npm start
```
Во втором терминале:
```
npm run start-app-dev
```

### Сборка production версии
`package-all` можно заменить на любой из:
- `package-linux`
- `package-mac`
- `package-win`

Целевые сборки будут находиться в папке `dist` в незапакованном виде.

#### Вариант с yarn (рекомендуется)
```
yarn
yarn build
yarn run package-all
```
#### Вариант с npm
```
npm install
npm build
npm run package-all
```
