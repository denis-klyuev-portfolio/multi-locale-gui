import React, {Component} from 'react';
import CodeMirror from 'react-codemirror';
import 'codemirror/mode/xml/xml';
import classnames from 'classnames';

import './App.css';
import "codemirror/lib/codemirror.css";
import "codemirror/theme/neo.css";

import fs from 'fs';
import path from 'path';
import $ from 'jquery';
import './polyfills';

import allLangs from './langs';
// console.log(allLangs, Object.keys(allLangs));

import {remote} from 'electron';
const dialog = remote.dialog;

class App extends Component {
	state = {
		folder: localStorage.lastDir || null,
		locales: {},
		visibleLocales: [],
		allKeys: [],
		selectedLocale: null,
		selectedId: null,
		isLoading: false
	};
	editors = {};
	onOpenIconTap = () => {
		dialog.showOpenDialog({
			title: 'Выберите папку',
			defaultPath: this.state.folder,
			properties: ['openDirectory'],
			filters: [
				{name: 'All Files', extensions: ['*']}
			]
		}, (files = []) => {
			if (!files.length) {
				return;
			}
			const [folder] = files;
			localStorage.lastDir = folder;
			this.loadFromDir(folder);
		});
	};
	onReopenIconTap = () => {
		this.loadFromDir(this.state.folder);
	};
	loadFromDir = (folder) => {
		if (!fs.statSync(folder).isDirectory()) {
			return;
		}

		const locales = fs
			.readdirSync(folder)
			.filter(file => file.endsWith('.xml'))
			.map(file => ({
				file,
				locale: file.match(/(.{2})\.[^\.]+$/)[1],
				data: $(fs.readFileSync(path.join(folder, file), 'UTF-8')).find('> string, > font').toArray().reduce((p, c) => ({
					...p, [$(c).attr('id') || ('_font_' + $(c).attr('src'))]: {
						tagName: c.tagName,
						value: c.innerHTML,
						attributes: Array.fromObject(c.attributes).map(({name, value}) => ({
							name,
							value
						})).reduce((p, c) => ({...p, [c.name]: c.value}), {})
					}
				}), {})
			}))
			.reduce((p, c) => ({...p, [c.locale]: c}), {});

		const allKeys = Object.keys(locales).map(locale => Object.keys(locales[locale].data)).flatten().unique((a, b) => a === b).filter(f => !/^_font_/.test(f));
		this.setState({
			visibleLocales: ['en', 'ru'],
			folder,
			allKeys,
			locales
		});
	};
	onSaveIconTap = () => {
		const saveFile = (locale) => {
			const data = this.state.locales[locale];
			const configElement = $('<config/>');
			const elements = Object.keys(data.data).map(key => $('<' + data.data[key].tagName + '/>').attr(data.data[key].attributes).html(data.data[key].value));
			const elementsSize = elements.length;
			configElement.append("\n");
			for (let i = 0; i < elementsSize; i++) {
				configElement.append("\t");
				configElement.append(elements[i]);
				configElement.append("\n");
			}
			fs.writeFileSync(
				path.join(this.state.folder, data.file),
				`<?xml version="1.0" encoding="UTF-8" ?>\n` + $('<div>').append(configElement.clone()).html(), 'UTF-8');
		};

		Object.keys(this.state.locales).forEach(locale => saveFile(locale));
	};
	handleLocaleIdValue2 () {

	}
	handleLocaleIdValue = (key, lang, value) => {
		this.setState({
			locales: {
				...this.state.locales,
				[lang]: {
					...this.state.locales[lang],
					data: {
						...this.state.locales[lang].data,
						[key]: {
							...this.state.locales[lang].data[key],
							value: value
						}
					}
				}
			}
		});
	};
	handleEditorKeyDown = (key, lang, e) => {
		if (e.target.selectionEnd === e.target.value.length) {
			switch (e.keyCode) {
				case 39:
					const langPosition = (this.state.visibleLocales.indexOf(lang) + 1) % this.state.visibleLocales.length;
					const newLang = this.state.visibleLocales[langPosition];
					this.editors[newLang][key].focus();
					break;
				case 40:
					const keyPosition = (this.state.allKeys.indexOf(key) + 1) % this.state.allKeys.length;
					const newKey = this.state.allKeys[keyPosition];
					this.editors[lang][newKey].focus();
					break;
			}
			//	39 ->
			//	37 <-
			//	38 up
			//	40 down
		} else if (e.target.selectionEnd === 0) {
			switch (e.keyCode) {
				case 37:
					const langPosition = (this.state.visibleLocales.indexOf(lang) - 1 + this.state.visibleLocales.length) % this.state.visibleLocales.length;
					const newLang = this.state.visibleLocales[langPosition];
					this.editors[newLang][key].focus();
					break;
				case 38:
					const keyPosition = (this.state.allKeys.indexOf(key) - 1 + this.state.allKeys.length) % this.state.allKeys.length;
					const newKey = this.state.allKeys[keyPosition];
					this.editors[lang][newKey].focus();
					break;
			}
		}
	};
	handleCellWasFocused = (lang, key) => {
		this.setState({
			selectedLocale: lang,
			selectedId: key
		});
	};
	handleAddLocale = (locale) => {
		this.setState({
			visibleLocales: [...this.state.visibleLocales, locale],
			locales: {
				...this.state.locales,
				[locale]: {
					file: `${locale}.xml`,
					data: {},
					locale
				}
			}
		});
	};
	copyIdFromTo = (key, fromLang, toLang) => {
		this.setState({
			locales: {
				...this.state.locales,
				[toLang]: {
					...this.state.locales[toLang],
					data: {
						...this.state.locales[toLang].data,
						[key]: {
							...this.state.locales[toLang].data[key],
							attributes: {
								id: key
							},
							value: this.state.locales[fromLang].data[key].value
						}
					}
				}
			}
		});
	};

	handleToggleLocaleVisibility(locale, value) {
		let visibleLocales = this.state.visibleLocales;

		if (value) {
			visibleLocales = [...visibleLocales, locale];
		} else {
			visibleLocales = visibleLocales.without(locale);
		}
		visibleLocales.sort();

		this.setState({visibleLocales});
	}

	handleInteract (cm) {
		console.log('handleInteract', cm.getValue());
	}

	deleteKey = (key) => {
		const newLocales = {...this.state.locales};
		Object.keys(newLocales).forEach(locale => {
			if (newLocales[locale] && newLocales[locale].data[key]) {
				delete newLocales[locale].data[key];
			}
		});

		this.setState({
			allKeys: this.state.allKeys.without(key),
			locales: newLocales
		});
	};

	render() {
		const langs = this.state.visibleLocales;

		return (
			<div className="app">
				{!!Object.keys(this.state.locales).length && <div className="locales-pane">
					<h3>Локали</h3>
					<ul>
						{Object.keys(this.state.locales).sorted().map(locale =>
							<li key={locale}>
								<label>
									<input type="checkbox"
										   checked={this.state.visibleLocales.includes(locale)}
										   onChange={(e) => this.handleToggleLocaleVisibility(locale, e.target.checked)}
									/>
									{locale}
								</label>
							</li>
						)}
						{Object.keys(allLangs)
							.filter(locale => !this.state.locales[locale])
							.sorted()
							.map(locale =>
							<li key={locale}>
								<button onClick={(e) => this.handleAddLocale(locale)}>+{locale}</button>
							</li>
						)}
					</ul>
				</div>}
				<div className="main">
					<button
						className="save"
						onClick={this.onSaveIconTap}
					>Сохранить все
					</button>
					<button
						className="open"
						onClick={this.onOpenIconTap}
					>Открыть папку
					</button>
					{!!Object.keys(this.state.locales).length && <button
						className="reopen"
						onClick={this.onReopenIconTap}
					>Переоткрыть
					</button>}
					{!!Object.keys(this.state.locales).length && <div className={classnames('table', {stretched: this.state.visibleLocales.length < 4})}>
						<div className="head">
							<div>id</div>
							{langs.map(lang => <div className={classnames({selected: this.state.selectedLocale === lang})} key={'col-lang-' + lang}>{lang}</div>)}
						</div>
						<div className="body">
						{this.state.allKeys.map(key =>
							<div key={'row-' + key}>
								<div key={'cell-' + key + '-lang-id'} className={classnames({selected: this.state.selectedId === key})}>
									{key}
									<button onClick={(e) => this.deleteKey(key)}>x</button>
								</div>
								{langs.map(lang => <div key={'cell-' + key + '-lang-' + lang}>
									{this.state.selectedLocale === lang && this.state.selectedId === key
										? <CodeMirror
										value={this.state.locales[lang].data[key] ? this.state.locales[lang].data[key].value : ''}
										onChange={value => this.handleLocaleIdValue(key, lang, value)}
										options={{
											mode: "xml",
											autofocus: true,
											matchBrackets: true,
											//lineNumbers: true,
											theme: 'neo',
											readonly: !this.state.locales[lang].data[key]
										}}
										//interact={this.handleInteract.bind(this)}
										//onFocusChange={() => this.handleCellWasFocused(lang, key)}
									/> :
										<textarea
											className={classnames({
												'equals-en': lang !== 'en' && this.state.locales[lang].data[key] && this.state.locales[lang].data[key].value == this.state.locales.en.data[key].value
											})}
											onClick={() => this.handleCellWasFocused(lang, key)}
											disabled={!this.state.locales[lang].data[key]}
											value={this.state.locales[lang].data[key] ? this.state.locales[lang].data[key].value : ''}/>
									}
									{!this.state.locales[lang].data[key] && <button onClick={(e) => this.copyIdFromTo(key, 'en', lang)}>Копировать из <b>en</b></button>}
								</div>)}
							</div>
						)}
						</div>
					</div>}
				</div>
			</div>
		);
	}
}

export default App;
