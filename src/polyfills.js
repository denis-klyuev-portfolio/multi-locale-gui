/* eslint-disable */
if (!Math.sign) {
	Math.sign = function (value) {
		if (value === 0) {
			return 0;
		}
		if (value < 0) {
			return -1;
		}
		return 1;
	};
}
Array.copiesOf = function (value, count) {
	var result = [];

	for (var i = 0; i < count; i++) {
		result[i] = value;
	}

	return result;
};
/**
 * Максимальный элемент массива
 * @returns {*}
 */
Array.prototype.max = function () {
	return Math.max.apply(Math, this);
};
/**
 * Минимальный элемент массива
 * @returns {*}
 */
Array.prototype.min = function () {
	return Math.min.apply(Math, this);
};
/**
 * Возвращает отсортированный массив. Аргументы проксируются в Array.prototype.sort
 * @return {Array}
 */
Array.prototype.sorted = function () {
	var newArray = this.slice();
	newArray.sort(...arguments);
	return newArray;
};
/**
 * Случайный элемент массива
 * @param {number=} n
 * @returns {*}
 */
Array.prototype.sample = function (n) {
	if (n === undefined) {
		return this[Math.round(Math.random() * (this.length - 1))];
	}
	else {
		var result = [], i;

		for (i = 0; i < n; i++) {
			var index;

			do {
				index = Math.round(Math.random() * (this.length - 1));
			} while (~result.indexOf(index) && result.length < this.length);

			result.push(index);
		}

		for (i = 0; i < result.length; i++) {
			result[i] = this[result[i]];
		}

		return result;
	}
};
/**
 * Возвращает плоскую копию массива: [[1], [2], [3]] => [1, 2, 3]
 * @param {boolean=} shallow
 * @returns {Array}
 */
Array.prototype.flatten = function (shallow) {
	var result = [];

	for (var i = 0; i < this.length; i++) {
		if (this[i] && this[i].constructor === Array) {
			result.push.apply(result, shallow ? this[i] : this[i].flatten());
		}
		else {
			result.push(this[i]);
		}
	}

	return result;
};
/**
 * Удаляет указанное значение
 * @param {*} value
 */
Array.prototype.remove = function (value) {
	var pos = this.indexOf(value);
	if (pos !== -1) {
		this.splice(pos, 1);
	}
};
/**
 * Возвращает массив без элементов, удовлетворяющих критерию filter. Для фильтрации используется universalFilter
 * @see universalFilter
 * @param filter
 * @return {Array.<*>}
 */
Array.prototype.removeAll = function (filter) {
	return this.filter(function (element) {
		return !universalFilter(filter, element);
	});
};
/**
 * Возвращает копию массива без указанного значения
 * @param {*} value
 * @returns {Array}
 */
Array.prototype.without = function (value) {
	return this.filter((element, index, array) => element !== value);
};
/**
 * Возвращает копию массива без дубликатов
 * @param {Function=} callback
 * @returns {Array}
 */
Array.prototype.unique = function (callback) {
	var result = [], i;

	if (!callback) {
		for (i = 0; i < this.length; i++) {
			if (~result.indexOf(this[i])) {
				result.push(this[i]);
			}
		}
	}
	else {
		for (i = 0; i < this.length; i++) {
			var found = false;
			for (var j = 0; j < result.length; j++) {
				found = found || callback(this[i], result[j]);
				if (found) {
					break;
				}
			}

			if (!found) {
				result.push(this[i]);
			}
		}
	}

	return result;
};
/**
 * Подсчитывает сумму элементов
 */
Array.prototype.sum = function () {
	var result = 0;

	for (var i = 0; i < this.length; i++) {
		result += this[i];
	}

	return result;
};
/**
 * Разбивает массив на куски по limit длиной каждый. Не изменяет текущий массив
 * @param {number} limit
 * @returns {Array[]} Возвращает разбитый массив
 */
Array.prototype.split = function (limit) {
	var result = [];

	for (var i = 0; i < this.length; i++) {
		var arrIndex = Math.floor(i / limit);

		if (result[arrIndex] === undefined) {
			result[arrIndex] = [];
		}

		result[arrIndex].push(this[i]);
	}

	return result;
};
/**
 * Возвращает массив с указанным свойством каждого элемента
 * @param {string...} props
 */
Array.prototype.pluck = function () {
	var props = Array.prototype.slice.apply(arguments);
	return this
		.map(function (el) {
			if (props.length > 1) {
				return props.reduce(function (prev, prop) {
					prev[prop] = el[prop];
					return prev;
				}, {});
			} else {
				return el[props[0]];
			}
		});
};
/**
 * Возвращает массив, каждый элемент которого умножен на коэффициент coef
 * @param {number} coef
 */
Array.prototype.mul = function (coef) {
	return this
		.map(function (el) {
			return el * coef;
		});
};
/**
 * Сравнивает массив с другим массивом
 * @param {Array} arr
 */
Array.prototype.equals = function (arr) {
	return JSON.stringify(this) === JSON.stringify(arr);
};
Array.range = function (r1, r2) {
	let inc = r1 > r2 ? -1 : 1,
		result = [], i;

	for (i = r1; i !== r2; i += inc) {
		result.push(i);
	}
	result.push(i);

	return result;
};
//	TODO: хрень
Array.fromObject = function (object) {
	let result = [];

	if (typeof object.length !== 'undefined') {
		for (let i = 0; i < object.length; i++) {
			result[i] = object[i];
		}
	}

	return result;
};

/**
 * Проверка на то что массивы одинаковы
 * @param a
 */
Array.prototype.isTheSame = function (a) {
	if (this.length !== a.length) {
		return false;
	}

	for (let i = 0; i < this.length; i++) {
		if (this[i] !== a[i]) {
			return false;
		}
	}
	return true;
};

Array.prototype.last = function () {
	return this.length ? this[this.length - 1] : undefined;
};

/**
 * Находит вхождение одного массива в другой
 * @param {array} subArray
 * @return {number}
 */
Array.prototype.indexOfArray = function (subArray) {
	for (var i = 0; i < this.length - subArray.length + 1; i++) {
		var found = true;

		for (var j = 0; j < subArray.length; j++) {
			found = found && this[i + j] === subArray[j];
		}

		if (found) {
			return i;
		}
	}

	return -1;
};

/**
 * Выбрать из диапазона значений
 * @param {number=} rangeIndex - процентное значение индекса массива, возможно от 0 до 100.
 * @returns {*}
 */
Array.prototype.pickFromRange = function (rangeIndex) {

	if (rangeIndex < 0) {
		rangeIndex = 0;
	}

	if (rangeIndex > 100) {
		rangeIndex = 100;
	}

	return this[Math.round((rangeIndex / 100) * (this.length - 1))];
};
Array.prototype.firstHigher = function (value) {

	var result = 0;

	for (var i = 0; i < this.length; i++) {
		result = Math.max(value, this[i]);

		if (result > value) {
			break;
		}
	}

	return result;
};
Number.prototype.px = function () {
	return String.format('{0}px', this);
};
Number.prototype.pc = function () {
	return String.format('{0}%', this);
};
Number.prototype.times = function (callback) {
	for (var i = 0; i < this; i++) {
		callback(i);
	}
};
/**
 * Число попадает в отрезок [min; max]
 * @param {number} min
 * @param {number} max
 * @return {boolean}
 */
Number.prototype.within = function (min, max) {
	return this >= min && this <= max;
};
/**
 * Число попадает в интервал (min; max)
 * @param {number} min
 * @param {number} max
 * @return {boolean}
 */
Number.prototype.inside = function (min, max) {
	return this > min && this < max;
};
/**
 *
 * @param {*} filter
 * @param {*} value
 * @returns {boolean}
 */
function universalFilter(filter, value) {
	if (!filter) {
		return false;
	} else if (filter.constructor === RegExp) {
		return filter.test(value);
	} else if (filter.constructor === Array) {
		return !!~filter.indexOf(value);
	} else if (filter.constructor === Function) {
		return filter(value);
	} else {
		return value === filter;
	}
}
if (!Object.pickValues) {
	Object.defineProperty(Object.prototype, 'pickValues', {
		writable: false,
		enumerable: false,
		value: function (filter) {
			return Object.keys(this)
				.filter(universalFilter.bind(null, filter))
				.reduce(function (totalCurrent, currentKey) {
					totalCurrent[currentKey] = this[currentKey];
					return totalCurrent;
				}.bind(this), {});
		}

	});
}
if (!Object.assign) {
	Object.defineProperty(Object, 'assign', {
		enumerable: false,
		configurable: true,
		writable: true,
		value: function (target, firstSource) {
			if (target === undefined || target === null) {
				throw new TypeError('Cannot convert first argument to object');
			}

			var to = Object(target);
			for (var i = 1; i < arguments.length; i++) {
				var nextSource = arguments[i];
				if (nextSource === undefined || nextSource === null) {
					continue;
				}

				var keysArray = Object.keys(Object(nextSource));
				for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
					var nextKey = keysArray[nextIndex];
					var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
					if (desc !== undefined && desc.enumerable) {
						to[nextKey] = nextSource[nextKey];
					}
				}
			}
			return to;
		}
	});
}
/**
 * Случайное целое числов между min и max
 * @param  {number} min - минимальное значение
 * @param  {number} max - максимальное значение
 * @return {number}
 */
Math.getRandomInt = function (min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

/**
 * Случайно число между min и max
 * @param  {number} min - минимальное значение
 * @param  {number} max - максимальное значение
 * @return {number}
 */
Math.getRandomArbitary = function (min, max) {
	return Math.random() * (max - min) + min;
};